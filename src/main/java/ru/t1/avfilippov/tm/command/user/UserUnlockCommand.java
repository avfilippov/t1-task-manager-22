package ru.t1.avfilippov.tm.command.user;

import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    private final String NAME = "user-unlock";

    private final String DESCRIPTION = "unlock user";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        getServiceLocator().getUserService().unlockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
