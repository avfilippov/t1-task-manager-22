package ru.t1.avfilippov.tm.api.service;

public interface IServiceLocator {

    IAuthService getAuthService();

    ICommandService getCommandService();

    ITaskService getTaskService();

    IProjectService getProjectService();

    ILoggerService getLoggerService();

    IProjectTaskService getProjectTaskService();

    IUserService getUserService();

}
